class AddFavoriteToFavorites < ActiveRecord::Migration[6.1]
  def change
    add_column :favorites, :favorite_book, :boolean
    add_column :favorites, :current_page, :integer
  end
end
