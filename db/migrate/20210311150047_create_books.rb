class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :title
      t.text :description
      t.string :img
      t.references :author, null: false, foreign_key: true
      t.datetime :published

      t.timestamps
    end
  end
end
