class AddAwardsToAuthors < ActiveRecord::Migration[6.1]
  def change
    add_column :authors, :awards, :string
    add_column :authors, :followers, :string
  end
end
