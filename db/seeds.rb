# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(firstname: "hamza",lastname: "dahmani",email:"hamza@test.com" ,password: "123")
author1 = Author.create(name: "JOAN DIDION", img: "https://images2.minutemediacdn.com/image/upload/c_fit,f_auto,fl_lossy,q_auto,w_728/v1555899320/shape/mentalfloss/gettyimages-153392074.jpg?itok=_nC6MQ7O" )
author2 = Author.create(name: "GEORGE R.R. MARTIN", img: "https://images2.minutemediacdn.com/image/upload/c_fit,f_auto,fl_lossy,q_auto,w_728/v1555899317/shape/mentalfloss/gettyimages-164018781.jpg?itok=dCfyewBQ")
author3 = Author.create(name: "EDWIDGE DANTICAT", img: "https://images2.minutemediacdn.com/image/upload/c_fit,f_auto,fl_lossy,q_auto,w_728/v1561997329/shape/mentalfloss/64532-gettyimages-493589424_1.jpg?itok=DEm1AfZn")