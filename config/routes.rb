Rails.application.routes.draw do
  resources :favorites
  resources :authors
  resources :book
  resources :followers
  resource :users, only: [:create]
  post "/login", to: "users#login"
  delete "/favorites_delete", to: "favorites#destroy"
  post "/author_list_books", to: "book#author_list_books"
  post "/add_or_update_favorite", to: "favorites#update"

  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
