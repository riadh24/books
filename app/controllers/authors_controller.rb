class AuthorsController < ApplicationController
    before_action :authorized

    def index
        @authors = Author.all 
        render json: @authors
    end 

    def show
        @author = Author.find(params[:id])
        render json: @author
    end 

    def create
        @author = Author.create(
            name: params[:name],
            img: params[:img],
            awards: params[:awards],
            followers: params[:followers]
        )
        render json: @author
    end 

    def update
        @author = Author.find(params[:id])
        @author.update(
            name: params[:name],
            img: params[:img],
            awards: params[:awards],
            followers: params[:followers]
        )
        render json: @author
    end 

    def destroy
        @author = Author.find(params[:id])
        @author.destroy
        render json: @author
    end 
end
