class FavoritesController < ApplicationController
  before_action :set_favorite, only: [:show, ]

  # GET /favorites
  def index
    arrayofBooks = [] 
    @user.favorite.each do |favoriteBook| 
      if (Book.find(favoriteBook[:book_id]))  
        arrayofBooks.push(Book.find(favoriteBook[:book_id])) 
      end
    end
    render json: arrayofBooks
  end

  # GET /favorites/1
  def show
    render json: @favorite
  end

  # POST /favorites
  def create
    @favorite = Favorite.new(favorite_params)

    if @favorite.save
      render json: @favorite, status: :created, location: @favorite
    else
      render json: @favorite.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /favorites/1
  def update
    @favorite =Favorite.where(book_id: params[:book_id] , user_id: params[:user_id])
    if @favorite.length != 0
      if @favorite.update(
      favorite_book: params[:favorite_book] ,
      current_page: params[:current_page] 
      )
      render json: @favorite
      else
      render json: @favorite.errors, status: :unprocessable_entity
      end
    else 
      create
    end 
  end

  # DELETE /favorites/{book_id}
  def destroy
    
    @favorite =Favorite.where(book_id: params[:book_id] , user_id: params[:user_id])
    @favorite.each do |element|
       element.delete  
    end
    render json:  {book_id: params[:book_id] , user_id: params[:user_id]}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favorite
      @favorite = Favorite.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def favorite_params
      params.require(:favorite).permit(:user_id, :book_id, :favorite_book, :current_page)
    end
end


