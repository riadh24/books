class BookController < ApplicationController
    before_action :authorized
 
    def index
        @book = Book.all
        values = []
             @book.each do |item| 
                values.push(item.attributes.merge(user_favorite: book_favorite(item['id']))) 
            end
        render :json => values
        
    end 

    def author_list_books
       @author = Author.find(params[:author_id])
        @book = @author.books
        render :json =>  @book 
    end 

    def show
        @book = Book.find(params[:id])
        res= @book.attributes.merge(user_favorite: book_favorite(@book['id']))
        @author = Author.find(@book.author_id)
        values = []
        render json:{ "book":  @book.attributes.merge(user_favorite: book_favorite( @book.id)) , "author": @author  }
    end 

    def create
        @author = Author.find(params[:author_id])
        @book = @author.books.create(book_params)
        if @book.save
            render json: @book, status: :created 
          else
            render json: @book.errors, status: :unprocessable_entity
          end
    end 

    def update
        @book = Book.find(params[:id])
        @book.update(
            title: params[:title],
            description: params[:description],
            img: params[:img]
        )
        render json: @book
    end 

    def destroy
        @book = Book.find(params[:id])
        @book.destroy
        render json: @book
    end 
    private

    def comment_params
        params.require(:book).permit(:commenter, :body)
      end
    def book_params
      params.require(:book).permit(:title, :description, :img,:author_id)
    end

 
    def book_favorite(data)
       
        exist = @user.favorite.where(book_id: data)
      
        if (exist.length != 0)  
            return {
            favorite_book: exist[0].favorite_book, 
            current_page: exist[0].current_page
            }
            else 
            return false
           end
        end  
     
end


