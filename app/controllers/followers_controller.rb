class FollowersController < ApplicationController
    before_action :set_follower, only: [:show, :update]
  
    # GET /Followers
    def index
        @author = Author.find(params[:author_id])
      arrayofFollowers = [] 
      @author.followers.each do |element| 
        if (User.find(element[:user_id]))  
            arrayofFollowers.push(User.find(element[:user_id])) 
        end
      end
      render json: arrayofFollowers
    end
  
    # GET /followers/1
    def show
      render json: @follower
    end
  
    # POST /followers
    def create
      @follower = Follower.new(follower_params)
  
      if @follower.save
        render json: @follower, status: :created, location: @follower
      else
        render json: @follower.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /followers/1
    def update
      if @follower.update(follower_params)
        render json: @follower
      else
        render json: @follower.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /followers
    def destroy
      @follower =Follower.where(book_id: params[:author_id] , user_id: params[:user_id])
      @follower.each do |element|
        element.destroy
      end
      render json: {message: "done !"}
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_follower
        @follower = Follower.find(params[:id])
      end
  
      # Only allow a list of trusted parameters through.
      def follower_params
        user_id = decoded_token[0]['user_id']
        params.require(:follower).permit(:user_id, :author_id)
      end
  end
  